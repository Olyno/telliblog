import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import users from 'users';
import cookieSession from 'cookie-session';
import * as sapper from '@sapper/server';

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

polka() // You can also use Express
	.use(bodyParser.json())
	.use(cookieSession({
		secret: 'telliblogProject',
		maxAge: new Date() + 1000 * 60 * 60 * 24 * 7 * 4 // ~ 1 month
	}))
	.use(
		helmet(),
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		sapper.middleware({
			session: (req, res) => ({
				user: req.session.user
			})
		})
	)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});
