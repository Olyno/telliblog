import safepass from 'safepass';
import users from 'users';

export async function post(req, res) {
	const { login, password } = req.body;
	let user = Array.from(users).find(u => u.login === login);
	let hashedPassword = await safepass.hash(password);
	if (user && (hashedPassword === user.password)) {
		user = JSON.stringify(user);
		req.session.user = user;
	} else {
		user = JSON.stringify({error: 'Login or password wrong!'});
		res.statusCode = 404;
	}
	res.end(user);
}