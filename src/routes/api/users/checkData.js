import users from 'users';

export async function post(req, res) {
	if (req.session.user !== undefined) {
		const u = JSON.stringify( users.get( JSON.parse(req.session.user).login ) );
		req.session.user = u;
		res.end(u);
	}
}