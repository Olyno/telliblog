import users from 'users';

export async function get(req, res) {
    res.end(JSON.stringify( [...users.values()] ));
}