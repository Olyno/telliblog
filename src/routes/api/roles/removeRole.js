import users from 'users';
import permissions from 'permissions';
import { validUser, hasMinimumPermissions } from 'checker';

export async function post(req, res) {
	const { user, role, apiToken } = req.body;
	const allowedUser = validUser(apiToken);
	if ( allowedUser && hasMinimumPermissions(allowedUser, permissions.REMOVE_ROLE, permissions.MANAGE_ROLES, permissions.ADMIN) ) {
		users.get(user).removeRole(role);
		res.end(JSON.stringify( {ok: true} ));
	} else {
		res.statusCode = 403;
		res.end(JSON.stringify( {error: 'You don t have the permission to do that!'} ))
	}
}